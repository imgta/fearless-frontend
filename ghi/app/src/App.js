import React from 'react';
import logo from './logo.svg';
import './App.css';
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConference';
import PresentationForm from './PresentationForm';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from './MainPage';


// Destructure 'attendees' object within App function parameters
function App({attendees}) {

  // On the 1st function call, there're no attendees properties to pass (undefined)
  // if (attendees===undefined) {
  //     return null; }

  // Sets attendees to = empty array if undefined so object can always be iterable
  attendees = attendees || [];

  return (
    <BrowserRouter>
      <Nav />
        <div className="container">
          <Routes>
          <Route index element={<MainPage />} />
              <Route path="locations">
                <Route path="new" element={<LocationForm />} />
              </Route>
              <Route path="conferences">
                <Route path="new" element={<ConferenceForm />} />
              </Route>
              <Route path="presentations">
                <Route path="new" element={<PresentationForm />} />
              </Route>
              <Route path="attendees">
                <Route path="" element={<AttendeesList attendees={attendees} />} />
                <Route path="new" element={<AttendConferenceForm />} />
              </Route>

          </Routes>
        </div>
    </BrowserRouter>
  );
}

export default App;
