import React, {useEffect, useState} from 'react';


function ConferenceForm() {
    const [name, setName] = useState('');
    const [starts, setStarts] = useState('');
    const [ends, setEnds] = useState('');
    const [description, setDescription] = useState('');
    const [maxPresentations, setMaxPresentations] = useState('');
    const [maxAttendees, setMaxAttendees] = useState('');
    const [locations, setLocations] = useState([]);
    const [location, setLocation] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);};
    const handleStartsChange = (event) => {
        const value = event.target.value;
        setStarts(value);};
    const handleEndsChange = (event) => {
        const value = event.target.value;
        setEnds(value);};
    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);};
    const handleMaxPresentationsChange = (event) => {
        const value = event.target.value;
        setMaxPresentations(value);};
    const handleMaxAttendeesChange = (event) => {
        const value = event.target.value;
        setMaxAttendees(value);};
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);};

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {
            name,
            starts,
            ends,
            description,
            max_presentations: maxPresentations,
            max_attendees: maxAttendees,
            location,
        };
        console.log(data);

        try {
            const conferenceUrl = 'http://localhost:8000/api/conferences/';
            const fetchConfig = {
                method: "POST",
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(conferenceUrl, fetchConfig);
            if(!response.ok) {
                throw new Error('Request has failed');
            }
            const newConference = await response.json();
            console.log(newConference);
            setName('');
            setStarts('');
            setEnds('');
            setDescription('');
            setMaxPresentations('');
            setMaxAttendees('');
            setLocation('');

        } catch (error) {
            console.error(error);
        }
    };

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await fetch('http://localhost:8000/api/locations/');
                if(!response.ok) {
                    throw new Error('Bad response');
                }
                const data = await response.json();
                setLocations(data.locations);
            } catch (error) {
                console.error(error);
            }
        };

        fetchData();
    }, []);

    return (
        <div className="my-5 container">
        <div className="row">
        <div className="col col-sm-auto">
            <img width="400" className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" alt="Logo" />
        </div>
        <div className="col">

            <div className="offset-0 col-8">
            <div className="shadow">
                <h1>Create a new conference</h1>
                <form onSubmit={handleSubmit} id="create-conference-form">
                <div className="form-floating mb-3">
                    <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" value={name} />
                    <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleStartsChange} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control" value={starts} />
                    <label htmlFor="starts">Starts</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleEndsChange} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control" value={ends} />
                    <label htmlFor="ends">Ends</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleDescriptionChange} placeholder="Description" required type="text" name="description" id="description" className="form-control" value={description} />
                    <label htmlFor="description">Description</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleMaxPresentationsChange} placeholder="Max Presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" value={maxPresentations} />
                    <label htmlFor="max_presentations">Max Presentations</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleMaxAttendeesChange} placeholder="Max Attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" value={maxAttendees} />
                    <label htmlFor="max_attendees">Max Attendees</label>
                </div>
                <div className="mb-3">
                    <select value={location} onChange={handleLocationChange} required name="location" id="location" className="form-select">
                    <option value="">Choose a location</option>
                        {locations.map((location) => (
                            <option key={location.id} value={location.id}>
                                {location.name}
                            </option>
                            ))}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
            </div>
            </div>
        </div>
    );
}

export default ConferenceForm;
