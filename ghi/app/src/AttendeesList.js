import React from 'react';


function AttendeesList({attendees}) {
    return (
        <>
        <div className="my-5 container">
        <div className="row">
        <div className="col col-sm-auto">
            <img width="400" className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" alt="Logo" />
        </div>
        <div className="col col-auto">
            <div className="offset-0 col-auto">
            <div className="shadow">
            <h1>List of Attendees</h1>
        <table className="table table-info table-hover mt-3">
            <thead>
                <tr>
                <th className="text-center">Name</th>
                <th className="text-center">Conference</th>
                </tr>
            </thead>
            <tbody>
                {attendees.map(attendee => {
                return (
                <tr key={attendee.href}>
                    <td className="text-center">{attendee.name}</td>
                    <td className="text-center">{attendee.conference}</td>
                </tr>
                );
            })}
            </tbody>
        </table>
        </div>
        </div>
        </div>
        </div>
        </div>
        </>
    );
}

export default AttendeesList;
