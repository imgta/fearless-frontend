import React, {useEffect, useState} from 'react';


function LocationForm() {
    const [states, setStates] = useState([]);
    const [state, setState] = useState('');
    const [name, setName] = useState('');
    const [roomCount, setRoomCount] = useState('');
    const [city, setCity] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);}
    const handleRoomCountChange = (event) => {
        const value = event.target.value;
        setRoomCount(value);}
    const handleCityChange = (event) => {
        const value = event.target.value;
        setCity(value);}
    const handleStateChange = (event) => {
        const value = event.target.value;
        setState(value);}

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/states/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setStates(data.states);
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {
            name, city, state,
            room_count: roomCount,
        };
        console.log(data);

        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const newLocation = await response.json();
            console.log(newLocation);

            setName('');
            setRoomCount('');
            setCity('');
            setState('');
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="my-5 container">
        <div className="row">
        <div className="col col-sm-auto">
            <img width="400" className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" alt="Logo" />
        </div>
        <div className="col">

            <div className="offset-0 col-8">
            <div className="shadow">
                <h1>Create a new location</h1>
                <form onSubmit={handleSubmit} id="create-location-form">
                <div className="form-floating mb-3">
                    <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" value={name} />
                    <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleRoomCountChange} placeholder="Room count" required type="number" name="room_count" id="room_count" className="form-control" value={roomCount} />
                    <label htmlFor="room_count">Room count</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleCityChange} placeholder="City" required type="text" name="city" id="city" className="form-control" value={city} />
                    <label htmlFor="city">City</label>
                </div>
                <div className="mb-3">
                    <select onChange={handleStateChange} required name="state" id="state" className="form-select">
                    <option value={state}>Choose a state</option>
                        {states.map((state) => (
                            <option key={state.abbreviation} value={state.abbreviation}>
                                {state.name}
                            </option>
                            )
                        )}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
            </div>
            </div>

        </div>
    );
}

export default LocationForm;
